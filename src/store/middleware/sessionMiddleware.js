import {
  ACTION_SESSION_INIT,
  ACTION_SESSION_SET,
  sessionSetAction,
} from "../actions/sessionAction";

export const sessionMiddleWare =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_SESSION_INIT) {
      //read localStorage
      const storedSession = localStorage.getItem("translation-ss");
      //check if session exists
      if (!storedSession) {
        return;
      }
      //if exists:
      const session = JSON.parse(storedSession);
      //console.log(session);
      dispatch(sessionSetAction(session));
    }

    if (action.type === ACTION_SESSION_SET) {
      //store the session somewhere
      console.log("Set session");
      console.log(action.payload);
      localStorage.setItem("translation-ss", JSON.stringify(action.payload));
    }
  };
