import { applyMiddleware } from "redux";
import { loginMiddleware } from "./loginMiddleware";
import { sessionMiddleWare } from "./sessionMiddleware";

export default applyMiddleware(loginMiddleware, sessionMiddleWare);
