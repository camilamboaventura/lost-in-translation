import { LoginAPI } from "../../components/Login/LoginAPI";
import {
  ACTION_LOGIN_ATTEMPT,
  ACTION_LOGIN_SUCCESS,
  loginErrorAction,
  loginSuccessAction,
} from "../actions/loginActions";
import { sessionSetAction } from "../actions/sessionAction";

//dispach is used to execute new actions, once the login is completed we can dispatch new action using these disruption
export const loginMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_LOGIN_ATTEMPT) {
      //make an http request to try and login
      LoginAPI.login(action.payload)
        .then((users) => {
          //login_success
          if (users.length === 0) {
            throw new Error("No user found"); //force it into a catch
          }
          dispatch(loginSuccessAction(users[0]));
        })
        .catch((error) => {
          //error
          dispatch(loginErrorAction(error.message));
        });
    }

    if (action.type === ACTION_LOGIN_SUCCESS) {
      //not logged in
      dispatch(sessionSetAction(action.payload));
    }
  };
