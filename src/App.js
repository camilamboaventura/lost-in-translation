import "./App.css";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import Login from "./components/Login/Login";
import AppContainer from "./hoc/AppContainer";
import Translation from "./components/Translation/Translation";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        {/* Here goes the routes */}
        <Switch>
          <Route path="/" exact component={Login}></Route>
          <Route path="/translation" component={Translation}></Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
