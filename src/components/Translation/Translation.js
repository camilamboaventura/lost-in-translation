import AppContainer from "../../hoc/AppContainer";
import signLanguage from "../../Images/signlanguage-removebg-preview.png";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";

function Translation() {
  console.log("Get session");
  const { name } = useSelector((state) => state.sessionReducer);

  return (
    <div>
      <nav className="navbar navbar-light bg-light">
        <div className="container-fluid">
          <div className="navbar-brand">
            <img src={signLanguage} alt="logo" className="logo" />
            Lost in Translation
          </div>
          <div>
            <NavLink className="nav-link d-flex" to="/profile">
              <span className="material-icons">account_circle</span>
              &nbsp;Welcome, {name}
            </NavLink>
          </div>
        </div>
      </nav>
      <AppContainer>
        <div className="input-group mb-3">
          <input
            type="text"
            className="form-control"
            placeholder="Add a text to translate"
            aria-label="Recipient's username"
            aria-describedby="button-addon2"
          />
          <button
            className="btn btn-outline-secondary"
            type="button"
            id="button-addon2"
          >
            Translate
          </button>
        </div>
      </AppContainer>
    </div>
  );
}

export default Translation;
