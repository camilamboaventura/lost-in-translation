export const LoginAPI = {
  login(credentials) {
    //making a request to server to verify if the name exists
    return fetch(`http://localhost:8000/users?name=${credentials.name}`).then(
      async (response) => {
        //se essa validacao é verdadeira retorna response.json()
        // console.log(response.json());
        // if (!response.body.length > 0) {
        //   const { error = "An unknown error ocurred" } = await response.json();
        //   throw new Error(error); //force it into a catch
        // }
        return response.json();
      }
    );
  },
};
