import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Redirect } from "react-router-dom";
import AppContainer from "../../hoc/AppContainer";
import { loginAttemptAction } from "../../store/actions/loginActions";

function Login() {
  const [credentials, setCredentials] = useState({
    name: "",
  });

  const dispach = useDispatch();
  const { loggedIn } = useSelector((state) => state.sessionReducer);
  const { loginError, loginAttempting } = useSelector(
    (state) => state.loginReducer
  );

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };

  const onFormSubmit = (event) => {
    event.preventDefault(); //stop the page reload
    dispach(loginAttemptAction(credentials)); //tenta logar - primeiro passo
  };

  return (
    <>
      {loggedIn && <Redirect to="/translation" />}{" "}
      {/* se estiver logado redirect para /translation */}
      {!loggedIn && (
        <AppContainer>
          <form className="mt-3 mb-3" onSubmit={onFormSubmit}>
            <h1>Login to Lost in Translation</h1>

            <div className="mb-3 ">
              <label htmlFor="name" className="form-label">
                Name
              </label>
              <input
                id="name"
                type="text"
                placeholder="What's your name"
                className="form-control"
                onChange={onInputChange}
              ></input>
            </div>

            <button type="submit" className="btn btn-primary btn-lg">
              Login
            </button>
          </form>

          {loginAttempting && <p>Try to login...</p>}

          {loginError && (
            <div className="alert alert-danger" role="alert">
              <h4>Unsuccessful</h4>
              <p className="mb-0">{loginError}</p>
            </div>
          )}
        </AppContainer>
      )}
    </>
  );
}

export default Login;
